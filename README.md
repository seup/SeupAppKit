
Any file starting with `mainHem_` is a sample app which is used to demo a particular feature of the app. 

`module` - contains hem modules necessary to create an app. Copy this folder to your app folder when creating your app.

Any file starting with `hem` is a module 

`data` is sample HEM data for apps which do plotting and data analysis. This is not required for apps to run. Just sample data to play with.s

----

`mainHem_bare.py` - boiler plate code for app 

`mainHem_pullData.py` - Pull node(all) data from HEMApp Server through API

`mainHem_pullData_single.py` - Pull single node data from HEMApp Server through API

`mainHem_actuate.py` -  Turn on/off nodes through API

`mainHem_demandManage.py` - Perform load management when power consumption crosses threshold

`mainHem_email.py` - Send an email from your app

`mainHem_demandManage_email.py` - Send an email from your app when you do load management

`mainHem_file.py` - Extract necessary information from data dump 

`mainHem_parsing.py`- Parsing data from data dump 

`mainHem_powerAggregate.py`- aggregate power data based on load/sources/charger for plotting and analytics

`mainHem_report.py` - create a pdf report
