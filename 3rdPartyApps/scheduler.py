import datetime
import threading 
import time 
from dateutil import parser

from module.hemClient import HemClient
import module.hemParsingData
import module.hemScheduleMgr
import module.hemEmail

SERVER_PORT = 9931
SERVER_NAME = '192.168.1.28'

# Create a new HEM Client
hemClient = HemClient()

#Create a new HEM Scheduler
hemScheduler=module.hemScheduleMgr.hemScheduleMgr('gridbox','192.168.1.28',9931, 8)

#Configure the server address 
# If you're running your app on a HEM and pulling data from same HEM, use SERVER_NAME='localhost'

server_address = (SERVER_NAME, SERVER_PORT)

#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()

		#logic/intelligence
		myManagementAlgorithm(responseData.values()[0])

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)



def main():
	file= open('schedule.csv','r')
	#format of schedule.csv file should be as mentioned below 
	#ACT:ON,node:0,date:01/09/2018-13:20:00
	
	lines=file.readlines()
	#Before you call this, make sure you have a file with the login details in the same directory as your app
	#In this case we are using 'loginDetails.log'
	#The file should have two lines
	
	#email:myemail@email.com
	#password: mypassword

	#This is the email and password of the HEM

	hemEmailAddr, hemPassword = module.hemEmail.getEmailLoginDetails('loginDetails.log')

	module.hemEmail.sendEmail(hemEmailAddr, hemPassword, 'thitheekshaattavar@gmail.com', 'hello', '\nScheduler App is running\n\n')

	for line in lines:

		data=line.split(",")
		act=data[0].split(":")
		nodenumber=data[1].split(":")

		date=data[2]
		dateAll=date

		dateSplit=dateAll.split("-")
		dateDate=data[2].split(":",1)

		dateTime=dateDate[1]

		try:
			dateData_format = parser.parse(dateTime)
			dateTimeNow = datetime.datetime.now()

			difference = dateTimeNow-dateData_format

			if (difference.total_seconds() > 0):

				print "Scheduled date is in the past"

			print act[1]
			print nodenumber[1]
			print dateData_format
			
			nodeName = 'l' + nodenumber[1] 
			print nodeName

			if act[1] == 'ON':
				print 'display On'
				hemScheduler.appendOnTime(nodeName, dateData_format)

			elif act[1] == 'OFF':
				print 'display OFF '
				hemScheduler.appendOffTime(nodeName, dateData_format)

			print(hemScheduler.getSchedule())

		except ValueError:
			print "Incorrect mm/dd/yr format"

		
		finally:
			hemScheduler.startExecution()


main()
