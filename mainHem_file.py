#
# Sample app to work on HEM data which is stored in the SD card
# This app will show you how to 
# 1. Extract cetain data from the SD card
# 2. Extract data based on time range 
# 3. Making a smaller copy of the main data dump based on a condition

# Author: Ashray Manur

import datetime
import threading
import time
import subprocess
from pathlib import Path


from module.hemClient import HemClient
import module.hemParsingData

SERVER_PORT = 9931


SERVER_NAME = 'localhost'

server_address = (SERVER_NAME, SERVER_PORT)


# Create a new HEM Client
hemClient = HemClient()


#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)


def main():

	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()

	#************************************************
	#You can put all of the following logic in a separate function
	#************************************************

	#HEM automatically records all data in 'data.csv'
	#This is a huge file. 
	#Let's say you want to extract specific contents from the file
	#instead of copying the entire file 
	#In this example, let's say the name of the file is 'sample.csv'
	# and it is in the ../data directory

	#following are some simple commands you can execute 

	#*****************************************************************
	#1. Copy only one day's worth of data from sample.csv to a new file oneday.csv
	cmd = "grep -e '10/1/17' ../data/sample.csv > ../data/oneDay.csv"
	#Need a better way to do this without using shell=True. For now, this will work
	result = subprocess.check_output(cmd, shell=True)

	#Check if the file exists. This is to make sure the file was created 
	fileExists = Path('../data/oneDay.csv')
	if(fileExists.is_file()):
		print 'file exists'

	#*****************************************************************
	#2. Copying one day's worth of only voltage data
	cmd = "grep -e 'VOLT.*10/1/17' ../data/sample.csv > ../data/oneDayVolt.csv"
	result = subprocess.check_output(cmd, shell=True)

	#Check if the file exists. This is to make sure the file was created 
	fileExists = Path('../data/oneDayVolt.csv')
	if(fileExists.is_file()):
		print 'file exists'

	#*****************************************************************
	#3. Copying one week worth of all data
	#This takes about 40 seconds on BBB
	cmd = "grep -e '10/[1-8]/17' ../data/sample.csv > ../data/oneWeek.csv"
	result = subprocess.check_output(cmd, shell=True)

	#Check if the file exists. This is to make sure the file was created 
	fileExists = Path('../data/oneWeek.csv')
	if(fileExists.is_file()):
		print 'file exists'


	#Similarly you can do various permutations and combinations of dates and data type
if __name__ == "__main__":
    main() 

