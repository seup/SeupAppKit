#
# A sample HEM App to turn on and off nodes

# Author: Ashray Manur

import datetime
import threading
import time


from module.hemClient import HemClient

SERVER_PORT = 9931
SERVER_NAME = 'localhost'

# Create a new HEM Client
hemClient = HemClient()


server_address = (SERVER_NAME, SERVER_PORT)

#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		responseData, addressInfo = hemClient.receiveResponse()

		#print complete response
		print responseData

		#print complete server details
		print addressInfo

		print responseData.keys() 

		print responseData.values()[0]
		#It returns a list of list
		#So just extract the first element in the outer list (which is also a list)

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)



def main():

	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()

	#Use this to send a request to server to pull power data for node 0
	#This can repeated for node 0-7
	sendToServer("/api/turnoff/3", server_address)
	time.sleep(1)

	#Use this to send a request to server to pull power data for node 3
	sendToServer("/api/turnon/3", server_address)
	time.sleep(1)

	#Similarly this can be done for nodes 0-7

if __name__ == "__main__":
    main() 

