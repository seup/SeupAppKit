#
# A sample HEM App to pull single node data from HEMApp when app is running on the same HEM
# This app shows how to pull power, voltage, current, charge etc. from HEMApp on a per node basis

# Author: Ashray Manur

import datetime
import threading
import time


from module.hemClient import HemClient

SERVER_PORT = 9931
SERVER_NAME = 'localhost'

# Create a new HEM Client
hemClient = HemClient()


#Configure the server address 
# If you're running your app on a HEM and pulling data from same HEM, use SERVER_NAME='localhost'
#If you're running your app on one HEM and pulling data from other HEMs, SERVER_NAME will not be 'localhost'
# It would be an ip address. For example SERVER_NAME = 192.168.1.28 

server_address = (SERVER_NAME, SERVER_PORT)

#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		#Call this to get a response. 
		#
		responseData, addressInfo = hemClient.receiveResponse()

		#print complete response
		print responseData

		#print complete server details
		print addressInfo

		print responseData.keys() 
		#[u'NODE', u'DCPOWER'] 
		#These are the keys

		print responseData.values()[0]
		#[[u'0', u'0.204375']]
		#These are the corresponding values for the keys
		#It returns a list of list
		#So just extract the first element in the outer list (which is also a list)

		print addressInfo[0]
		#192.168.1.28
		#This is the IP address of the server

		print addressInfo[1]
		#9931
		#This is the port number of the server

#This is a function. Call this only when you need to send a request to server
def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)



def main():

	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()

	#Use this to send a request to server to pull power data for node 0
	#This can repeated for node 0-7
	sendToServer("/api/getdcpower/0", server_address)
	time.sleep(1)

	#Use this to send a request to server to pull power data for node 3
	sendToServer("/api/getdcpower/3", server_address)
	time.sleep(1)


	#Use this to send a request to server to pull voltage data for node 2
	#This can repeated for node 0-7
	sendToServer("/api/getdcvoltage/2", server_address)
	time.sleep(1)

	#Use this to send a request to server to pull voltage data for node 5
	sendToServer("/api/getdcvoltage/5", server_address)
	time.sleep(1)

	#Use this to send a request to server to pull current data for node 0
	sendToServer("/api/getdccurrent/0", server_address)
	time.sleep(1)

	#Use this to send a request to server to pull current data for node 3
	sendToServer("/api/getdccurrent/3", server_address)
	time.sleep(1)


if __name__ == "__main__":
    main() 

