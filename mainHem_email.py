#
# Boilerplate code for app to deployed on HEM

# Author1: Ashray Manur
# Author2:

import datetime
import threading
import time


from module.hemClient import HemClient
import module.hemParsingData
import module.hemEmail

SERVER_PORT = 9931


SERVER_NAME = 'localhost'

server_address = (SERVER_NAME, SERVER_PORT)

# Create a new HEM Client
hemClient = HemClient()


#Data reception should always be a thread
def receiveFromServer():

	#This ensures this thread is called repeatedly
	# We want to keep our ears open for any data from server
	while True:

		
		responseData, addressInfo = hemClient.receiveResponse()


def sendToServer(apiRequest, serverAddress):

	hemClient.sendRequest(apiRequest, serverAddress)


def main():

	threads = []
	t = threading.Thread(target=receiveFromServer)
	threads.append(t)
	t.start()


	#Before you call this, make sure you have a file with the login details in the same directory as your app
	#In this case we are using 'loginDetails.log'
	#The file should have two lines
	
	#email:myemail@email.com
	#password: mypassword

	#This is the email and password of the HEM

	hemEmailAddr, hemPassword = module.hemEmail.getEmailLoginDetails('loginDetails.log')

	module.hemEmail.sendEmail(hemEmailAddr, hemPassword, 'manur@wisc.edu', 'hello', '\nis it working or no???\n\n')


if __name__ == "__main__":
    main() 

