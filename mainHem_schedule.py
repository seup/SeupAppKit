#from module.hemScheduleMgr import hemScheduleMgr
import module.hemScheduleMgr
import datetime
import time

def main():
    mgr1=module.hemScheduleMgr.hemScheduleMgr('gridbox','192.168.7.2',9931, 8)
    now=datetime.datetime.now()
    on1=now+datetime.timedelta(0,5)
    on2=now+datetime.timedelta(0,15)
    on3=now+datetime.timedelta(0,25)
    off1=now+datetime.timedelta(0,35)

    #t1=now.replace(hour=23, minute=10, second=0, microsecond=0)

    mgr1.appendOnTime('l0', on1)
    #print(mgr1.getSchedule())
    
    mgr1.appendOnTime('l1', on2)


    mgr1.appendOnTime('l2',on3)

    mgr1.appendOffTime('l2',off1)

    mgr1.setUpdateInterval(1)
    mgr1.startExecution()
    #print(mgr1.getLoadState())
    print(mgr1.getSchedule())
    #time.sleep(30)
    #print(mgr1.getLoadState())

if __name__ == "__main__":
    main() 
